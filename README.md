# NRL Web Components

A set of hand crafted web components using only standard Web APIs and vanilla JS.

## Installation

Using your preferred package manager:

```bash
npm install @nrl/web-components
# or
yarn add @nrl/web-components
```

## Development

Here are the instructions to setup your development environment:

```
$ git clone git@gitlab.com:NicolasRichel/nrl-web-components.git
$ cd nrl-web-components
$ yarn install
$ mkdir dev && touch dev/index.html dev/index.css dev/main.js
$ echo -e "\
<html lang="en">\n\
<head>\n\
  <meta charset="UTF-8">\n\
  <meta name="viewport" content="width=device-width, initial-scale=1.0">\n\
  <title>NRL Web Components</title>\n\
  <link rel="stylesheet" href="index.css" />\n\
  <script type="module" src="dist/main.js"></script>\n\
</head>\n\
<body>\n\
  <h1>Web components dev playground</h1>\n\
</body>\n\
</html>\n\
" > dev/index.html
$ echo "import '../index.js';" > dev/main.js
$ yarn dev
```

You can now open you favorite web browser and go to `http://localhost:8080`.

Once you edited some components code you can reload the page to see your changes.

## License

The content of this repository is published under the terms of the [GNU LGPL v3](./LICENSE.txt).
