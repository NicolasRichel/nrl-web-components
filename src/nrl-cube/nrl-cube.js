import { createShadowDOM } from '@nrl/web-component-utils';

const ELEMENT_NAME = 'nrl-cube';

const template = document.createElement('template');
template.innerHTML = `
<div class="scene">
  <div class="cube">
    <div class="face front">front</div>
    <div class="face back">back</div>
    <div class="face left">left</div>
    <div class="face right">right</div>
    <div class="face top">top</div>
    <div class="face bottom">bottom</div>
  </div>
</div>
<style>
:host {
  box-sizing: border-box;
  display: block;
}

.scene {
  width: 200px;
  height: 200px;
  perspective: 600px;
}

.cube {
  position: relative;
  width: 100%;
  height: 100%;
  transform-style: preserve-3d;
  transform: translateZ(-100px);
  transition: transform 0.5s;
}

.face {
  position: absolute;
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid #000;
  background-color: #DDD;
  font-weight: bold;
}

.face.front  { transform: rotateY(  0deg) translateZ(100px); }
.face.back   { transform: rotateY(180deg) translateZ(100px); }
.face.left   { transform: rotateY(-90deg) translateZ(100px); }
.face.right  { transform: rotateY( 90deg) translateZ(100px); }
.face.top    { transform: rotateX( 90deg) translateZ(100px); }
.face.bottom { transform: rotateX(-90deg) translateZ(100px); }

.cube.show-front  { transform: translateZ(-100px) rotateY(   0deg); }
.cube.show-back   { transform: translateZ(-100px) rotateY(-180deg); }
.cube.show-left   { transform: translateZ(-100px) rotateY(  90deg); }
.cube.show-right  { transform: translateZ(-100px) rotateY( -90deg); }
.cube.show-top    { transform: translateZ(-100px) rotateX( -90deg); }
.cube.show-bottom { transform: translateZ(-100px) rotateX(  90deg); }
</style>
`;

class NrlCube extends HTMLElement {

  constructor() {
    super();
    createShadowDOM(this, template);
  }

  connectedCallback() {
    this.cube = this.shadowRoot.querySelector('.cube');
    this.cube.classList.add('show-front');
    this.face = 'front';
  }

  show(face) {
    this.cube.classList.replace(`show-${this.face}`, `show-${face}`);
    this.face = face;
  }

}

window.customElements.define(ELEMENT_NAME, NrlCube);

export default NrlCube;
