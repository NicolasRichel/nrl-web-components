import { createShadowDOM } from '@nrl/web-component-utils';

const ELEMENT_NAME = 'nrl-terminal';
const CONFIG = {
  defaultWidth: '500px',
  defaultHeight: '400px',
  defaultPrompt: '>'
};

const template = document.createElement('template');
template.innerHTML = `
<div class="window">
  <div class="head"></div>
  <div class="body">
    <div class="input-line">
      <span class="prompt"></span>
      <input class="command input" />
    </div>
  </div>
</div>
<style>
* {
  box-sizing: border-box;
}

:host {
  display: block;
}

.window {
  border: 1px solid #000;
}

.head {
  height: 32px;
  padding: 8px;
  background-color: #DEDEDE;
  font-family: sans-serif;
  font-weight: bold;
}

.body {
  height: calc(100% - 32px);
  padding: 0 4px;
  background-color: #121212;
  overflow-y: auto;
  word-break: break-word;
  font-family: monospace;
  font-size: 16px;
  color: #FFF;
}

.input-line {
  display: flex;
  min-height: 24px;
  padding: 2px 0;
}

.input-line .prompt {
  padding: 0 4px;
  margin-right: 4px;
}

.input-line .command {
  flex-grow: 1;
  padding: 0;
}

.input-line .input {
  min-width: 0;
  outline: none;
  border: none;
  background: transparent;
  font-size: 16px;
  font-family: monospace;
  color: #FFF;
}
</style>
`;

class NrlTerminal extends HTMLElement {

  constructor() {
    super();
    this.history = [];
    this.historyIndex = -1;
    createShadowDOM(this, template);
  }

  get width() {
    return this.getAttribute('width') || CONFIG.defaultWidth
  }

  get height() {
    return this.getAttribute('height') || CONFIG.defaultHeight
  }

  get title() {
    return this.getAttribute('title');
  }

  get promptString() {
    return this.getAttribute('prompt') || CONFIG.defaultPrompt;
  }

  connectedCallback() {
    this.window = this.shadowRoot.querySelector('.window');
    this.head = this.shadowRoot.querySelector('.head');
    this.body = this.shadowRoot.querySelector('.body');
    this.prompt = this.shadowRoot.querySelector('.prompt');
    this.input = this.shadowRoot.querySelector('.input');

    this.window.style.width = this.width;
    this.window.style.height = this.height;
    this.head.textContent = this.title;
    this.prompt.textContent = this.promptString;
    this.body.addEventListener('click', () => this.input.focus());
    this.input.addEventListener('keyup', e => this.onKeyup(e));
  }

  onKeyup(e) {
    e.preventDefault();
    switch (e.key) {
      case 'Enter':
        this._handleCommand();
        break;
      case 'ArrowUp':
        this._historyBack();
        break;
      case 'ArrowDown':
        this._historyNext();
        break;
    }
  }

  attach(handler) {
    this.commandHandler = handler;
  }

  detach() {
    this.commandHandler = undefined;
  }

  async _handleCommand() {
    const input = this.input.value;

    if (input) {
      this.history.push(input);
      const event = new Event('command-input');
      event.value = input;
      this.dispatchEvent(event);
    }

    const inputLine = createInputLine(this.promptString, input);

    if (input && this.commandHandler) {
      this.input.disabled = true;
      let output;
      try {
        output = await this.commandHandler(input);
        const event = new Event('command-output');
        event.value = output;
        this.dispatchEvent(event);
      } catch (error) {
        output = `![Error]: ${error || 'Unknown error.'}`;
      }
      const ouputLine = createOutputLine(output);
      this.body.insertBefore(inputLine, this.input.parentNode);
      this.body.insertBefore(ouputLine, this.input.parentNode);
      this.input.disabled = false;
      this.input.focus();
    } else {
      this.body.insertBefore(inputLine, this.input.parentNode);
    }

    this.input.value = '';
    this.currentInput = '';
  }

  _historyBack() {
    if (this.historyIndex === -1) {
      this.currentInput = this.input.value;
      this.historyIndex = this.history.length - 1;
    } else if (this.historyIndex === 0) {
      // Do nothing
    } else if (this.historyIndex > 0) {
      this.historyIndex--;
    }
    this.input.value = this.history[this.historyIndex] || '';
  }

  _historyNext() {
    if (this.historyIndex === -1) {
      // Do nothing
    } else if (this.historyIndex < this.history.length - 1) {
      this.historyIndex++;
    } else if (this.historyIndex === this.history.length - 1) {
      this.historyIndex = -1;
    }
    this.input.value = this.history[this.historyIndex] || this.currentInput || '';
  }

}

function createInputLine(promptString, input) {
  const prompt = document.createElement('span');
  prompt.classList.add('prompt');
  prompt.textContent = promptString;

  const command = document.createElement('span');
  command.classList.add('command');
  command.textContent = input;

  const line = document.createElement('div');
  line.classList.add('input-line');
  line.appendChild(prompt);
  line.appendChild(command);

  return line;
}

function createOutputLine(output) {
  const line = document.createElement('div');
  line.classList.add('output-line');
  line.textContent = output;

  return line;
}

window.customElements.define(ELEMENT_NAME, NrlTerminal);

export default NrlTerminal;
